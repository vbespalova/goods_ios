//
//  DatabaseManager.h
//

#import <Foundation/Foundation.h>

#import "DatabaseManagerConfigurator.h"
#import "NSManagedObject+Parsing.h"

@interface ManagedObjectContext : NSManagedObjectContext

@end


@interface DatabaseManager : NSObject

@property (nonatomic, strong, readonly) NSManagedObjectContext *privateMOC;
@property (nonatomic, strong, readonly) NSManagedObjectContext *mainMOC;

+ (void)attachConfigurator:(DatabaseManagerConfigurator *)connfigurator;
+ (DatabaseManagerConfigurator *)configurator;

+ (instancetype)sharedManager;

+ (NSError *)removeLocalDatabase;
- (void)dropDatabaseWithCompletion:(void(^)(NSError *)) completion;

- (NSManagedObject<NSManagedObjectParsing> *)getObjectWithIdentifier:(id)identifier entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc error:(NSError **)error;
- (NSArray *)getObjectsWithEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc error:(NSError **)error;

- (id)getObjectWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc;
- (id)getObjectWithPredicate:(NSPredicate *)predicate entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc;

// Insert Or Update style
- (NSArray *)createObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context error:(NSError **)error;

// Удалит объекты, которые не вошли в апдейт.
- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName error:(NSError **)error;
- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName error:(NSError **)error shouldClear:(BOOL)shouldClear;

// Удалит объекты, которые не вошли в апдейт.
- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error;

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error context:(NSManagedObjectContext *)context;

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear;

// Удалит старые объекты, которые не вошли в range только в том случае, если range.location == 0 и последний из новых объектов не найден в старых.
- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName error:(NSError **)error;
- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName error:(NSError **)error shouldClear:(BOOL)shouldClear;

// Удалит старые объекты, которые не вошли в range только в том случае, если range.location == 0 и последний из новых объектов не найден в старых.
- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error;

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear;

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear context:(NSManagedObjectContext *)context;

- (void)deleteObjectsWithEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate error:(NSError **)error;

/* Batch Updates */

- (void)beginBatchUpdateForContext:(NSManagedObjectContext *)context;

- (void)endBatchUpdates;

/*
 Context saving
 */

- (void)savePrivateContext;

// Performing async blocks
- (void)performAsyncBlock:(void (^)(void))block withCompletion:(void(^)(void))completion;

- (NSDictionary *)totalObjectsCountInfo;

@end
