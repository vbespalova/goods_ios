//
//  NSManagedObject+Parsing.m
//

#import "NSManagedObject+Parsing.h"
#import "DatabaseManagerConfigurator.h"
#import "DatabaseManager.h"

@implementation NSManagedObject (Parsing)

- (NSError *)_updateWithParameters:(NSDictionary *)parameters ignoreNulls:(BOOL)ignoreNulls
{
    NSEntityDescription *entity = [self entity];
    NSDictionary *attributes = [entity attributesByName];
    DatabaseManagerConfigurator *configurator = [DatabaseManager configurator];
    NSError *error = nil;
    @try {
        for (NSString *attribute in attributes) {
            NSString *convertedAttribute = [configurator convertedAttributeName:attribute];
            if (convertedAttribute == nil) {
                convertedAttribute = [configurator convertedAttributeName:attribute forEntity:entity.name];
            }
            NSAssert(convertedAttribute, @"Attribute name is missing, check configurator");
            id value = parameters[convertedAttribute];
            if ([value isKindOfClass:[NSNull class]]) {
                value = nil;
            }
            // не будем сохранять то что не изменилось.
            id old_value = [self valueForKey:attribute];
            if (![old_value isEqual:value]) {
                if (value || old_value) {
                    if (ignoreNulls) {
                        if (value) {
                            [self setValue:value forKey:attribute];
                        }
                    }else{
                        [self setValue:value forKey:attribute];
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        error = [NSError errorWithDomain:@"com.goods.database" code:-100 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"%@: %@",exception.name, exception.reason]}];
    }
    return error;
};

- (NSError *)_updateWithParameters:(NSDictionary *)parameters
{
    return [self _updateWithParameters:parameters ignoreNulls:NO];
};

- (NSDictionary *)allParameters
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSDictionary *attributes = [self.entity attributesByName];
    for (NSString *attribute in attributes) {
        id value = [self valueForKey:attribute];
        if (value) {
            parameters[attribute] = value;
        }
    }
    return parameters;
};

- (BOOL)isIdentical:(id)object
{
    return [[self allParameters] isEqualToDictionary:[object allParameters]];
};

- (BOOL)isOrdered
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    
    return [self respondsToSelector:@selector(index)];
#pragma clang diagnostic pop
};

@end
