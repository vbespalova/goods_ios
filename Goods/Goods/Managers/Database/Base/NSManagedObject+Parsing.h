//
//  NSManagedObject+Parsing.h
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Parsing)

// обновление параметров "в лоб"
- (NSError *)_updateWithParameters:(NSDictionary *)parameters;

// ignoreUpdateIfNewIsNil - отвечает за то, что если вдруг параметр пришле nil (или не пришел) мы не затираем предыдущий параметр.
- (NSError *)_updateWithParameters:(NSDictionary *)parameters ignoreNulls:(BOOL)ignoreNulls;

- (NSDictionary *)allParameters;
- (BOOL)isIdentical:(id)object;

- (BOOL)isOrdered;

@end

@protocol NSManagedObjectParsing <NSObject>

- (NSError *)updateWithParameters:(NSDictionary *)parameters;

@end