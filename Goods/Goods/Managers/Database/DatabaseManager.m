//
//  DatabaseManager.m
//

#import "DatabaseManager.h"

@interface NSArray (Extra)

- (id)randomObject;
- (NSDictionary *)dictionaryWithKeyPath:(NSString *)keyPath;

@end

#define VERIFY_THREAD if (self.concurrencyType == NSMainQueueConcurrencyType) {NSAssert([NSThread isMainThread], @"Attempt to call Main MOC method not inside Main Thread!");} else { NSAssert(![NSThread isMainThread], @"Attempt to call Private MOC method inside Main Thread!");};
@implementation ManagedObjectContext : NSManagedObjectContext
#ifdef DEBUG

- (BOOL)hasChanges
{
    VERIFY_THREAD;
    return [super hasChanges];
};

- (NSManagedObject *)objectRegisteredForID:(NSManagedObjectID *)objectID
{
    VERIFY_THREAD;
    return [super objectRegisteredForID:objectID];
};

- (NSManagedObject *)objectWithID:(NSManagedObjectID *)objectID
{
    VERIFY_THREAD;
    return [super objectWithID:objectID];
};

- (NSManagedObject*)existingObjectWithID:(NSManagedObjectID*)objectID error:(NSError**)error
{
    VERIFY_THREAD;
    return [super existingObjectWithID:objectID error:error];
}

- (NSArray *)executeFetchRequest:(NSFetchRequest *)request error:(NSError **)error
{
    VERIFY_THREAD;
    return [super executeFetchRequest:request error:error];
};

- (NSUInteger) countForFetchRequest: (NSFetchRequest *)request error: (NSError **)error
{
    VERIFY_THREAD;
    return [super countForFetchRequest:request error:error];
}

- (void)insertObject:(NSManagedObject *)object
{
    VERIFY_THREAD;
    [super insertObject:object];
};

- (void)deleteObject:(NSManagedObject *)object
{
    VERIFY_THREAD;
    [super deleteObject:object];
};

- (void)refreshObject:(NSManagedObject *)object mergeChanges:(BOOL)flag
{
    VERIFY_THREAD;
    [super refreshObject:object mergeChanges:flag];
};

- (void)detectConflictsForObject:(NSManagedObject *)object
{
    VERIFY_THREAD;
    [super detectConflictsForObject:object];
};

// key-value observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    VERIFY_THREAD;
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
};

- (void)processPendingChanges
{
    VERIFY_THREAD;
    [super processPendingChanges];
};

- (void)assignObject:(id)object toPersistentStore:(NSPersistentStore *)store
{
    VERIFY_THREAD;
    [super assignObject:object toPersistentStore:store];
};

- (NSSet *)insertedObjects
{
    VERIFY_THREAD;
    return [super insertedObjects];
};
- (NSSet *)updatedObjects
{
    VERIFY_THREAD;
    return [super updatedObjects];
};
- (NSSet *)deletedObjects
{
    VERIFY_THREAD;
    return [super deletedObjects];
};
- (NSSet *)registeredObjects
{
    VERIFY_THREAD;
    return [super registeredObjects];
};

- (void)undo
{
    VERIFY_THREAD;
    [super undo];
};
- (void)redo
{
    VERIFY_THREAD;
    [super redo];
};
- (void)reset
{
    VERIFY_THREAD;
    [super reset];
};
- (void)rollback
{
    VERIFY_THREAD;
    [super rollback];
};
- (BOOL)save:(NSError **)error
{
    VERIFY_THREAD;
    return [super save:error];
};
#endif
@end


@interface DatabaseManager()

@property (nonatomic, strong, readonly) NSManagedObjectModel *model;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSManagedObjectContext *batchUpdatesContext;

@end

@implementation DatabaseManager

@synthesize privateMOC = _privateMOC;
@synthesize mainMOC = _mainMOC;
@synthesize coordinator = _coordinator;
@synthesize model = _model;

static DatabaseManager *s_DatabaseManagerInstance;
static DatabaseManagerConfigurator *s_DatabaseManagerConfigurator;


static dispatch_queue_t s_private_dispatch_queue = nil;

- (dispatch_queue_t)private_dispatch_queue
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_private_dispatch_queue = dispatch_queue_create("com.db.private", NULL);
    });
    
    return s_private_dispatch_queue;
}


+ (void)attachConfigurator:(DatabaseManagerConfigurator *)connfigurator
{
    s_DatabaseManagerConfigurator = connfigurator;
};

+ (DatabaseManagerConfigurator *)configurator
{
    return s_DatabaseManagerConfigurator;
};

+ (id)sharedManager
{
    if (!s_DatabaseManagerInstance) {
        s_DatabaseManagerInstance = [[DatabaseManager alloc] init];
    }
    return s_DatabaseManagerInstance;
}

- (id)init
{
    if (self = [super init])
    {
        NSAssert(s_DatabaseManagerConfigurator, @"Please attach DatabaseManagerConfigurator.");
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMocDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:self.privateMOC];
    }
    return self;
}
#pragma mark - Core Data Stack

- (NSManagedObjectContext *)privateMOC
{
    if (_privateMOC != nil) {
        return _privateMOC;
    }
    NSPersistentStoreCoordinator *coordinator = self.coordinator;
    if (coordinator != nil) {
        _privateMOC = [[ManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_privateMOC performBlockAndWait:^{
            [_privateMOC setPersistentStoreCoordinator:coordinator];
        }];
    }
    return _privateMOC;
}

- (NSManagedObjectContext *)mainMOC
{
    if (_mainMOC != nil) {
        return _mainMOC;
    }
    
    NSPersistentStoreCoordinator *coordinator = self.coordinator;
    if (coordinator != nil) {
        _mainMOC = [[ManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_mainMOC performBlockAndWait:^
         {
             [_mainMOC setPersistentStoreCoordinator:coordinator];
         }];
    }
    return _mainMOC;
}

- (void)onMocDidSave:(NSNotification *)notification
{
    if (_mainMOC)
    {
        [self.mainMOC performBlock:^{
            [self.mainMOC mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}

- (NSManagedObjectModel *)model
{
    if (_model != nil) {
        return _model;
    }
    NSString *databaseName = [s_DatabaseManagerConfigurator databaseName];
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:databaseName withExtension:@"momd"];
    _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _model;
}

+ (NSURL *)persistentStoreURL
{
    NSString *databaseName = [s_DatabaseManagerConfigurator databaseName];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", databaseName]];
    return storeURL;
}

- (NSPersistentStoreCoordinator *)coordinator
{
    if (_coordinator != nil) {
        return _coordinator;
    }
    
    NSError *error = nil;
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    if (![_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                    configuration:nil
                                              URL:[self.class persistentStoreURL]
                                          options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
                                            error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _coordinator;
}

+ (NSError *)removeLocalDatabase
{
    NSError *error = nil;
    NSURL *url = [self persistentStoreURL];
    [self removeStoreWithURL:url error:&error];
    
    return error;
}

+ (void)removeStoreWithURL:(NSURL *)url error:(NSError **)error
{
    [[NSFileManager defaultManager] removeItemAtURL:url error:error];
    NSString *path = [url path];
    NSString *databaseName = [path lastPathComponent];
    path = [path stringByDeletingLastPathComponent];
    
    NSArray *contents = [[NSFileManager defaultManager] subpathsAtPath:path];
    for (NSString *subpath in contents) {
        if ([subpath hasPrefix:databaseName]) {
            [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:subpath] error:error];
        }
    }
}

- (void)dropDatabaseWithCompletion:(void(^)(NSError *))completion
{
    __block NSError* error = nil;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[self privateMOC] performBlockAndWait:^{
            [[self privateMOC] reset];
            _privateMOC = nil;
            dispatch_semaphore_signal(semaphore);
        }];
    });
    
    dispatch_semaphore_wait(semaphore, 10);
    
    [[self mainMOC] performBlockAndWait:^{
        [self.mainMOC reset];
        _mainMOC = nil;
    }];
    
    for (NSPersistentStore *store in self.coordinator.persistentStores) {
        [self.coordinator removePersistentStore:store error:&error];
        [self.class removeStoreWithURL:store.URL error:&error];
    }
    
    _coordinator = nil;
    _model = nil;
    s_DatabaseManagerInstance = nil;
    
    if (completion) {
        completion(error);
    }
}

#pragma mark Common
- (NSManagedObject<NSManagedObjectParsing> *)getObjectWithIdentifier:(id)identifier entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc error:(NSError **)error
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    request.includesSubentities = [DatabaseManager configurator].shouldIncludeSubentities;
    request.predicate = [NSPredicate predicateWithFormat:@"identifier == %@", identifier];
    request.fetchLimit = 1;
    
    __block NSManagedObject<NSManagedObjectParsing>* object = nil;
    __block NSError *err = nil;
    [moc performBlockAndWait:^{
        NSArray *objects = [moc executeFetchRequest:request error:&err];
        if ([objects count] == 1) {
            object = objects[0];
        }
    }];
    *error = err;
    if (!err) {
        return object;
    }
    return nil;
};

- (NSArray *)getObjectsWithEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc error:(NSError **)error
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [request setIncludesPropertyValues:NO];
    request.includesSubentities = [DatabaseManager configurator].shouldIncludeSubentities;
    __block NSArray* objects = nil;
    __block NSError *err = nil;
    [moc performBlockAndWait:^{
        objects = [moc executeFetchRequest:request error:&err];
    }];
    if (error) {
        *error = err;
    }
    
    if (!err) {
        return objects;
    }
    return nil;
};

- (id)getObjectWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [request setIncludesPropertyValues:NO];
    request.includesSubentities = [DatabaseManager configurator].shouldIncludeSubentities;
    request.predicate = predicate;
    request.fetchLimit = 1;
    request.sortDescriptors = sortDescriptors;
    
    __block NSArray* objects = nil;
    __block NSError *err = nil;
    [moc performBlockAndWait:^{
        objects = [moc executeFetchRequest:request error:&err];
    }];
    
    if (!err && objects.count == 1) {
        return objects[0];
    }
    return nil;
};

- (id)getObjectWithPredicate:(NSPredicate *)predicate entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)moc
{
    return [self getObjectWithPredicate:predicate sortDescriptors:nil entityName:entityName inContext:moc];
};

- (NSArray *)createObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context error:(NSError **)error
{
    return [self createObjects:objectsParameters previousObjects:nil entityName:entityName inContext:context error:error];
}

- (NSArray *)createObjects:(NSArray *)objectsParameters previousObjects:(NSArray *)previousObjects entityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context error:(NSError **)error
{
    __block NSMutableArray *objects = [NSMutableArray array];

    [context performBlockAndWait:^{
        NSString *measureKey = [NSString stringWithFormat:@"OBJECTS_UPDATE (%@)", entityName];
        DatabaseManagerConfigurator *configurator = [[self class] configurator];
        NSString *convertedAttributeName = [configurator convertedAttributeName:@"identifier"];
        if (convertedAttributeName == nil) {
            convertedAttributeName = [configurator convertedAttributeName:@"identifier" forEntity:entityName];
        }
        NSAssert(convertedAttributeName, @"Attribute name is missing, check configurator");
        
        NSDictionary *previousObjectsDictionary = nil;
        if (previousObjects) {
            previousObjectsDictionary = [previousObjects dictionaryWithKeyPath:@"identifier"];
        }
        
        for (NSDictionary *objectParameters in objectsParameters) {
            NSDictionary *parameters = objectParameters;
            
            NSString *identifier = parameters[convertedAttributeName];
            NSManagedObject<NSManagedObjectParsing> *object = nil;
            if (identifier) {
                if (previousObjectsDictionary) {
                    object = previousObjectsDictionary[identifier];
                }
                if (!object) {
                    object = [self getObjectWithIdentifier:identifier entityName:entityName inContext:context error:error];
                    if(*error){
                        objects = nil;
                        return;
                    }
                }
            }else{
                if ([entityName hasSuffix:@"_Single"]) {
                    NSError *localError = nil;
                    NSArray *singleObjects = [self getObjectsWithEntityName:entityName inContext:context error:&localError];
                    NSLog(@"%@: ERROR: %@", self.class, localError.localizedDescription);
                    if (singleObjects.count == 1) {
                         object = [singleObjects lastObject];
                    }else if (singleObjects.count > 1) {
                        NSAssert(NO, @"Too Many Single Objects!");
                    }
                }
            }
            if (!object) {
                object = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
            }
            if (![object conformsToProtocol:@protocol(NSManagedObjectParsing)]) {
                objects = nil;
                return;
            }
            *error = [object updateWithParameters:parameters];
            if (*error) {
                objects = nil;
                return;
            }
            if ([object isOrdered]) {
                [object setValue:@([objectsParameters indexOfObject:objectParameters]) forKey:@"index"];
            }
            [objects addObject:object];
        }
    }];
    
    return objects;
};

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName error:(NSError **)error
{
    [self updateObjects:objectsParameters entityName:entityName error:error shouldClear:YES];
};

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName error:(NSError **)error shouldClear:(BOOL)shouldClear
{
     NSRange range = NSMakeRange(0, 0);
    [self updateObjects:objectsParameters range:range entityName:entityName error:error shouldClear:shouldClear];
}

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error context:(NSManagedObjectContext *)context
{
    [self updateObjects:objectsParameters range:NSMakeRange(0, 0) entityName:entityName forParentObjectWithID:parentObjectID parentObjectEntityName:parentObjectEntityName attributeName:attributeName error:error shouldClear:YES context:context];
};

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error
{
    [self updateObjects:objectsParameters entityName:entityName forParentObjectWithID:parentObjectID parentObjectEntityName:parentObjectEntityName attributeName:attributeName error:error shouldClear:YES];
}

- (void)updateObjects:(NSArray *)objectsParameters entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear
{
    NSRange range = NSMakeRange(0, 0);
    
    [self updateObjects:objectsParameters range:range entityName:entityName forParentObjectWithID:parentObjectID parentObjectEntityName:parentObjectEntityName attributeName:attributeName error:error shouldClear:shouldClear];
};

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName error:(NSError **)error
{
    [self updateObjects:objectsParameters range:range entityName:entityName error:error shouldClear:YES];
}

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName error:(NSError **)error shouldClear:(BOOL)shouldClear
{
    NSManagedObjectContext *context = self.privateMOC;
    [context performBlockAndWait:^{
        void(^failBlock)(void) = ^{
            [context rollback];
        };
        
        NSMutableArray *oldObjects = nil;
        if (range.location == 0) {
            
            oldObjects = [[self getObjectsWithEntityName:entityName inContext:context error:error] mutableCopy];
            if (*error) {
                failBlock();
                return;
            }
        }
        
        NSArray *previousObjects = nil;
        
        if([[self.class configurator] shouldUsePrevousObjectForUpdateEntityWithName:entityName]){
            previousObjects = oldObjects;
        }
        
        NSArray *objects = [self createObjects:objectsParameters previousObjects:previousObjects entityName:entityName inContext:context error:error];
        
        BOOL shouldClearOldObjects = (range.length == 0);
        
        if (!shouldClearOldObjects) {
            if ([objects count] > 0) {
                shouldClearOldObjects = ![oldObjects containsObject:[objects lastObject]];
            }
        }
        shouldClearOldObjects = shouldClearOldObjects && shouldClear;
        
        
        if (range.location == 0 && shouldClearOldObjects) {
            for (NSManagedObject *object in objects) {
                [oldObjects removeObject:object];
            }
            if (oldObjects.count > 0) {
                NSLog(@"REMOVING OLD OBJECTS %@ (%@)", entityName, @(oldObjects.count));
                for (NSManagedObject *object in oldObjects) {
                    [context deleteObject:object];
                }
            }
            
        }
        
        if (!*error) {
             if ([context hasChanges] && (context != self.batchUpdatesContext)) {
                [context save:error];
            }
        }
        else {
            failBlock();
        }
    }];
}

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error
{
    [self updateObjects:objectsParameters range:range entityName:entityName forParentObjectWithID:parentObjectID parentObjectEntityName:parentObjectEntityName attributeName:attributeName error:error shouldClear:YES];
}

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear context:(NSManagedObjectContext *)context
{
    BOOL bShouldSave = (context == nil);
    
    if (!context) {
        context = self.privateMOC;
    }
    
    [context performBlockAndWait:^{
        void(^failBlock)(void) = ^{
            [context rollback];
        };
        
        NSManagedObject<NSManagedObjectParsing> *parentObject = [self getObjectWithIdentifier:parentObjectID entityName:parentObjectEntityName inContext:context error:error];
        if (*error) {
            failBlock();
            return;
        }
        
        if ([parentObject conformsToProtocol:@protocol(NSManagedObjectParsing)]) {
            
            NSMutableSet *oldObjects = nil;
            if (range.location == 0) {
                id values = [parentObject valueForKey:attributeName];
                if ([values isKindOfClass:[NSSet class]]) {
                    oldObjects = [values mutableCopy];
                }else if(values){
                    oldObjects = [NSMutableSet setWithObject:values];
                }
            }
            
            NSArray *objects = [self createObjects:objectsParameters entityName:entityName inContext:context error:error];
            if (!*error) {
                NSDictionary *dict = [parentObject.entity relationshipsByName];
                __block BOOL isRelationToMany = NO;
                [dict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSRelationshipDescription *descr, BOOL *stop) {
                    if ([key isEqualToString:attributeName]) {
                        isRelationToMany = descr.isToMany;
                        *stop = YES;
                    }
                }];
                if (isRelationToMany) {
                    NSSet *setOfObjects = [NSSet setWithArray:objects];
                    [parentObject setValue:setOfObjects forKey:attributeName];
                }
                else {
                    [parentObject setValue:objects.firstObject forKeyPath:attributeName];
                }
            }else{
                failBlock();
                return;
            }
            
            BOOL shouldClearOldObjects = (range.length == 0) && shouldClear;
            
            if (!shouldClearOldObjects) {
                if ([objects count] > 0) {
                    shouldClearOldObjects = ![oldObjects containsObject:[objects lastObject]];
                }
            }
            shouldClearOldObjects = shouldClearOldObjects && shouldClear;
            
            if (range.location == 0 && shouldClearOldObjects) {
                
                for (NSManagedObject *object in objects) {
                    [oldObjects removeObject:object];
                }
                
                if (oldObjects.count > 0) {
                    NSLog(@"REMOVING OLD OBJECTS %@ (%@) FROM %@ WITH ID %@", entityName, @(oldObjects.count), parentObjectEntityName, parentObjectID);
                    for (NSManagedObject *object in oldObjects) {
                        [context deleteObject:object];
                    }
                }
            }
            
            if (!*error) {
                if ([context hasChanges] && (context != self.batchUpdatesContext) && bShouldSave) {
                    [context save:error];
                }
            }
            else {
                failBlock();
            }
        }
        
    }];
}

- (void)updateObjects:(NSArray *)objectsParameters range:(NSRange)range entityName:(NSString *)entityName forParentObjectWithID:(NSString *)parentObjectID parentObjectEntityName:(NSString *)parentObjectEntityName attributeName:(NSString *) attributeName error:(NSError **)error shouldClear:(BOOL)shouldClear
{
    [self updateObjects:objectsParameters range:range entityName:entityName forParentObjectWithID:parentObjectID parentObjectEntityName:parentObjectEntityName attributeName:attributeName error:error shouldClear:shouldClear context:nil];
}

- (void)deleteObjectsWithEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate error:(NSError **)error
{
    NSManagedObjectContext *context = self.privateMOC;
    [context performBlockAndWait:^{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
        request.includesSubentities = [DatabaseManager configurator].shouldIncludeSubentities;
        request.predicate = predicate;
        request.includesPropertyValues = NO;
        
        NSArray *objects = [context executeFetchRequest:request error:error];
        if (objects.count > 0) {
            
            for (NSManagedObject *object in objects) {
                [context deleteObject:object];
            }
            
            if (!*error) {
                if ([context hasChanges] && (context != self.batchUpdatesContext)) {
                    [context save:error];
                }
            }
            
            if (*error) {
                [context rollback];
            }
        }
    }];
}

- (NSDictionary *)totalObjectsCountInfo
{
    NSArray *entities = self.model.entities;
    static NSDictionary *s_totalObjectsCountInfo = nil;
    static NSInteger s_total = 0;
    NSMutableDictionary *totalObjectsCountInfo = [NSMutableDictionary dictionary];
    NSInteger total = 0;
    for (NSEntityDescription* entity in entities) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entity.name];
        NSError *error = nil;
        NSInteger count = [[[DatabaseManager sharedManager] privateMOC] countForFetchRequest:request error:&error];
        total += count;
        NSInteger newObjects = 0;
        if (s_totalObjectsCountInfo[entity.name]) {
            NSInteger previousCount = [s_totalObjectsCountInfo[entity.name][@"count"] integerValue];
            newObjects = count - previousCount;
        }
        totalObjectsCountInfo[entity.name] = @{@"count":@(count), @"new": @(newObjects)};
    }
    totalObjectsCountInfo[@"!ALL"] = @{@"count":@(total), @"new":@(total - s_total)};
    s_total = total;
    s_totalObjectsCountInfo = totalObjectsCountInfo;
    return totalObjectsCountInfo;
};

#pragma mark -Batch Updates

- (void)beginBatchUpdateForContext:(NSManagedObjectContext *)context
{
    [context performBlockAndWait:^{
        self.batchUpdatesContext = context;
    }];
}

- (void)endBatchUpdates
{
    [self.batchUpdatesContext performBlockAndWait:^{
        if (self.batchUpdatesContext) {
            
            if (self.batchUpdatesContext.hasChanges) {
                NSError *error = nil;
                [self.batchUpdatesContext save:&error];
                if (error) {
                    NSLog(@"DB BATCH ERROR %@",error);
                }
            }
        }
        self.batchUpdatesContext = nil;
    }];
}

#pragma mark - Context saving

- (void)savePrivateContext
{
    __block NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.privateMOC;

    [managedObjectContext performBlockAndWait:^{
        if ([managedObjectContext hasChanges]) {
            [managedObjectContext save:&error];
            
            if (error != nil) {
                NSLog(@"Core data: error while saving private context %@, %@", error, [error userInfo]);
                NSLog(@"Rolling back");
                [managedObjectContext rollback];
            }
        }
    }];
}

// Performing async blocks
- (void)performAsyncBlock:(void (^)(void))block withCompletion:(void(^)(void))completion
{
    dispatch_async([self private_dispatch_queue], ^{
        if (block) {
            block();
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    });
};

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

@implementation NSArray (Extra)

- (id)randomObject
{
    if (!self.count) return nil;
    
    NSUInteger index = (NSUInteger)arc4random_uniform((u_int32_t)self.count);
    
    return self[index];
}

- (NSDictionary *)dictionaryWithKeyPath:(NSString *)keyPath
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:self.count];
    for (id object in self) {
        @try {
            id key = [object valueForKey:keyPath];
            if (key) {
                dictionary[key] = object;
            }
        }
        @catch (NSException *exception) {
            
        }
    }
    return dictionary;
};

@end
