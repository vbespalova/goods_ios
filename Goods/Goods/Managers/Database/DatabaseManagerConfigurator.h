//
//  DatabaseManagerConfigurator.h
//

#import <Foundation/Foundation.h>

@interface DatabaseManagerConfigurator : NSObject

- (NSString *)databaseName;

- (NSString *)convertedAttributeName:(NSString *)attribute;

- (NSString *)convertedAttributeName:(NSString *)attribute forEntity:(NSString *)entity;

- (BOOL)shouldUsePrevousObjectForUpdateEntityWithName:(NSString *)entityName;

@property (atomic, readwrite) BOOL shouldIncludeSubentities;

@end
