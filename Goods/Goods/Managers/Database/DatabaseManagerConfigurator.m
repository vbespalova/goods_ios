//
//  DatabaseManagerConfigurator.m
//

#import "DatabaseManagerConfigurator.h"

@implementation DatabaseManagerConfigurator

- (NSString *)databaseName
{
    NSAssert(NO, @"Should be overriden!");
    return nil;
};

- (NSString *)convertedAttributeName:(NSString *)attribute
{
    return attribute;
};

- (NSString *)convertedAttributeName:(NSString *)attribute forEntity:(NSString *)entityName
{
    return attribute;
};

- (BOOL)shouldUsePrevousObjectForUpdateEntityWithName:(NSString *)entityName
{
    return NO;
};

@end
