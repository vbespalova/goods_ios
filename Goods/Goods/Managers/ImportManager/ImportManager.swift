//
//  ImportManager.swift
//  Goods

import Foundation
import CSwiftV
import Alamofire
import SWXMLHash

class ImportManager {
    static let sharedManager = ImportManager()
    
    func importCSV(completion : Optional<(NSError?) -> () >) {
        if let filepath = NSBundle.mainBundle().pathForResource("data", ofType: "csv") {
            do {
                var string = try NSString(contentsOfFile: filepath, usedEncoding: nil) as String
                string = "name,price,count\n\(string)"
                let csv = CSwiftV(string: string)
                var objects : [[String : AnyObject]] = []
                for row in csv.keyedRows! {
                    var mRow = row
                    mRow["identifier"] = NSUUID().UUIDString
                    objects.append(mRow)
                }
                
                var error : NSError?
                DatabaseManager.sharedManager().performAsyncBlock({ () -> Void in
                    DatabaseManager.sharedManager().updateObjects(objects, entityName: kEntityItem, error: &error)
                    }, withCompletion: { () -> Void in
                        if completion != nil {
                            completion?(nil)
                        }
                })
            } catch {
                if completion != nil {
                    completion?(NSError(description:"Не удалось загрузить файл"))
                }
            }
        } else {
            if completion != nil {
                completion?(NSError(description:"Не удалось загрузить файл"))
            }
        }
    }
    
    func importJSON(completion : Optional<(NSError?) -> () >) {
        Alamofire.request(
            .GET,
            "https://demo4852155.mockable.io/goods/json",
            parameters: nil,
            encoding: .JSON)
            .validate()
            .responseJSON {(response) in
                if response.result.isSuccess {
                    if let result = response.result.value as? [AnyObject] {
                        var error : NSError?
                        DatabaseManager.sharedManager().performAsyncBlock({ () -> Void in
                            DatabaseManager.sharedManager().updateObjects(result, entityName: kEntityItem, error: &error)
                            }, withCompletion: { () -> Void in
                                if completion != nil {
                                    completion?(nil)
                                }
                        })
                    } else {
                        if completion != nil {
                            completion?(NSError(description: "Полученные данные имеют некорректный формат"))
                        }
                    }
                } else {
                    if completion != nil {
                        completion?(response.result.error)
                    }
                }
        }
    }
    
    
    func importXML(completion : Optional<(NSError?) -> () >) {
        Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = [
            "Content-Type": "text/html"
        ]
        Alamofire.request(
            .GET,
            "https://demo4852155.mockable.io/googs/xml",
            parameters: nil,
            encoding: .JSON)
            .validate()
            .response {(request, response, data, error) in
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                    if error == nil && data != nil {
                        let xml = SWXMLHash.parse(data!)
                        var objects : [AnyObject] = []
                        for element in xml.children[0].children {
                            var parameters = [String : AnyObject]()
                            parameters["identifier"] = element["identifier"].element?.text ?? ""
                            parameters["name"] = element["name"].element?.text ?? ""
                            parameters["price"] = element["price"].element?.text ?? 0
                            parameters["count"] = element["count"].element?.text ?? 0
                            objects.append(parameters)
                        }
                        var error : NSError?
                        DatabaseManager.sharedManager().updateObjects(objects, entityName: kEntityItem, error: &error)
                        if completion != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion?(error)
                            })
                        }
                    } else {
                        if completion != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion?(error ?? NSError(description: "Не удалось загрузить объекты"))
                            })
                        }
                    }
                })
        }
    }
}



