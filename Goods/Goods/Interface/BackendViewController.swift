//
//  SecondViewController.swift
//  Goods

import UIKit
import SwiftSpinner

class BackendViewController: BaseViewController , UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    lazy var adapter: DatabaseFetchAdapter = {
        var adapter = DatabaseFetchAdapter(entityName: kEntityItem, predicate: nil, sortDescriptors: [NSSortDescriptor(key: "name", ascending: true)], moc: DatabaseManager.sharedManager().mainMOC)
        adapter.tableView = self.tableView
        adapter.emptyView = self.emptyLabel
        weak var __w_self = self
        adapter.onFetched = {
            () -> () in
            if __w_self != nil {
                var isEmpty = __w_self!.adapter.fetchedObjectsCount == 0
                 __w_self!.tableView.separatorStyle = isEmpty ? UITableViewCellSeparatorStyle.None : UITableViewCellSeparatorStyle.SingleLine
                __w_self!.navigationItem.leftBarButtonItem!.enabled = !isEmpty
                if (isEmpty && __w_self!.editing) {
                    __w_self!.setEditing(false, animated: false)
                }
            }
        }
        return adapter;
    }()
    
    // MARK: View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        adapter.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        adapter.viewWillAppear()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        adapter.viewWillDisappear()
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.editing = editing
    }
    
    // MARK: Acions
    
    @IBAction func onAdd(sender: AnyObject) {
        openItem(nil)
    }
    
    @IBAction func onAction(sender: AnyObject) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        actionSheet.addAction(UIAlertAction(title: "Загрузить CSV", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
            SwiftSpinner.show("Загрузка...", animated: false)
            ImportManager.sharedManager.importCSV({ (error) -> () in
                error?.handle()
                SwiftSpinner.hide()
            })
        }))
        actionSheet.addAction(UIAlertAction(title: "Загрузить XML", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) ->Void in
            SwiftSpinner.show("Загрузка...", animated: false)
            ImportManager.sharedManager.importXML({ (error) -> () in
                error?.handle()
                SwiftSpinner.hide()
            })
        }))
        actionSheet.addAction(UIAlertAction(title: "Загрузить JSON", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) ->Void in
            SwiftSpinner.show("Загрузка...", animated: false)
            ImportManager.sharedManager.importJSON({ (error) -> () in
                error?.handle()
                SwiftSpinner.hide()
             })
        }))
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openItem(item : Item!) {
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("Edit") as! ItemEditViewController
        controller.item = item
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return adapter.sectionsCount
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adapter.rowsCountInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let item = adapter.objectAtIndexPath(indexPath) as! Item
        cell?.textLabel?.text = item.name
        cell?.detailTextLabel?.text = "\(item.count ?? 0) шт."
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = adapter.objectAtIndexPath(indexPath) as! Item
        openItem(item)
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let item = adapter.objectAtIndexPath(indexPath) as! Item
            if let identifier = item.identifier {
                DatabaseManager.sharedManager().performAsyncBlock({ () -> Void in
                    var error :NSError?
                    DatabaseManager.sharedManager().deleteObjectsWithEntityName(kEntityItem, predicate: NSPredicate(format: "identifier = %@", identifier), error: &error)
                    }, withCompletion: { () -> Void in
                })
            }
        }
    }
}

