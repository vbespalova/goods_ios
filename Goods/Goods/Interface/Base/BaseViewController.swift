//
//  BaseViewController.swift
//  Goods

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    deinit {
        print("\(self.classForCoder.description()) deinit")
    }
    
    var preferrableFirstResponders : [UIView]! {
        return nil
    }
    
    var keyboardAvoidableScrollView : UIScrollView! {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(self.classForCoder.description()) viewDidLoad")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let keyboardAvoidableScrollView = self.keyboardAvoidableScrollView
        if keyboardAvoidableScrollView != nil {
            keyboardAvoidableScrollView.setKeyboardAvoidingEnabled(true)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        let preferrableFirstResponders = self.preferrableFirstResponders
        if preferrableFirstResponders != nil && preferrableFirstResponders.count > 0 {
            self.view.endEditing(true)
        }
        
        let keyboardAvoidableScrollView = self.keyboardAvoidableScrollView
        if keyboardAvoidableScrollView != nil {
            keyboardAvoidableScrollView.setKeyboardAvoidingEnabled(false)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let preferrableFirstResponders = self.preferrableFirstResponders
        if preferrableFirstResponders != nil && preferrableFirstResponders.count > 0 {
            var alreadyHasFirstResponder = false
            for responder in preferrableFirstResponders {
                if  responder.isFirstResponder() {
                    alreadyHasFirstResponder = true
                    break
                }
            }
            if !alreadyHasFirstResponder {
                preferrableFirstResponders.first?.becomeFirstResponder()
            }
        }
        
        let keyboardAvoidableScrollView = self.keyboardAvoidableScrollView
        if keyboardAvoidableScrollView != nil {
            keyboardAvoidableScrollView.updateKeyboardAvoiding()
        }
    }
}