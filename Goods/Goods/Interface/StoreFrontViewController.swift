//
//  FirstViewController.swift
//  Goods


import UIKit

class StoreFrontViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    lazy var adapter: DatabaseFetchAdapter = {
        var adapter = DatabaseFetchAdapter(entityName: kEntityItem, predicate: NSPredicate(format: "count > 0"), sortDescriptors: [NSSortDescriptor(key: "name", ascending: true)], moc: DatabaseManager.sharedManager().mainMOC)
        weak var __w_self = self
        adapter.onFetched = {
            () -> () in
            if __w_self != nil {
                __w_self?.createControllers()
            }
        }
        return adapter;
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
        adapter.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        adapter.viewWillAppear()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        adapter.viewWillDisappear()
    }

    func createControllers () {
        var viewControllers : [UIViewController]?
        if adapter.fetchedObjectsCount == 0 {
            viewControllers = [(storyboard?.instantiateViewControllerWithIdentifier("Empty"))!]
        } else {
            var shouldReload = true
            
            if let currentController = self.viewControllers?.first as? StoreFrontItemViewController {
                if self.index(currentController) != nil {
                    currentController.reloadData()
                    shouldReload = false
                }
            }
            
            if shouldReload {
                viewControllers = [viewController(0)!]
            }
        }
        if viewControllers != nil {
            setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        }
    }

    // MARK : PageConroller
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let index = self.index(viewController as? StoreFrontItemViewController)
        if (index <= 0 || index == nil) {
            return nil
        } else {
            return self.viewController(index! - 1)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let index = self.index(viewController as? StoreFrontItemViewController)
        if (index == nil) {
            return nil
        } else {
            return self.viewController(index! + 1)
        }
    }
    
    func createController(item : Item) -> StoreFrontItemViewController {
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("StoreFrontItem") as! StoreFrontItemViewController
        controller.item = item;
        return controller
    }
    
    func index(controller: StoreFrontItemViewController?) -> Int? {
        if let item = controller?.item {
            return adapter.fetchedObjects.indexOf({ $0 as? Item == item})
        }
        return nil
    }
    
    func viewController(index :Int) -> StoreFrontItemViewController! {
        if index < adapter.fetchedObjectsCount {
            return createController((adapter.objectAtIndexPath(NSIndexPath(forItem: index, inSection: 0))) as! Item)
        }
        return nil
    }
}

class StoreFrontItemViewController : BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var item : Item!
    
    lazy var items : [[String : String]]! = {
        if self.item != nil {
            return [["title" : self.item.name ?? ""],
                ["title" : "Цена", "subtitle" : self.item.priceString],
                ["title" : "Количество", "subtitle" : "\(self.item.count!)"]
            ]
        } else {
            return []
        }
    }()
    
    @IBAction func onBuy(sender: AnyObject) {
        if let identifier = item.identifier {
            DatabaseManager.sharedManager().performAsyncBlock({ () -> Void in
                let moc = DatabaseManager.sharedManager().privateMOC
                moc.performBlockAndWait({ () -> Void in
                    do {
                        let item = try DatabaseManager.sharedManager().getObjectWithIdentifier(identifier, entityName: kEntityItem, inContext: moc) as? Item
                        if item != nil {
                            item?.count = (item?.count?.integerValue)! - 1
                            if moc.hasChanges {
                                try moc.save()
                            }
                        }
                    } catch {
                        moc.rollback()
                    }
                })
                }) { () -> Void in
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let item = items[indexPath.row]
        cell!.textLabel?.text = item["title"]
        cell!.detailTextLabel?.text = item["subtitle"]
        return cell!
    }
    
    func reloadData() {
        items = nil
        tableView.reloadData()
    }
}
