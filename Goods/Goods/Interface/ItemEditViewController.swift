//
//  ItemEditViewController.swift
//  Goods

import Foundation
import UIKit

class ItemEditViewController:BaseViewController {
    @IBOutlet weak var nameField: FieldView!
    @IBOutlet weak var priceField: FieldView!
    @IBOutlet weak var countField: FieldView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var item : Item! {
        didSet {
            if item == nil {
                 title = "Новый товар"
            } else {
                 title = item.name
            }
        }
    }
    
    func priceString(string : String) -> String? {
        if string.rangeOfString(Item.currency) == nil {
            return nil
        }
        var priceString = string.stringByReplacingOccurrencesOfString(Item.currency, withString: "")
        priceString = priceString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return priceString
    }
    
    override var keyboardAvoidableScrollView : UIScrollView! {
        return scrollView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateSaveButton()
        weak var __w_self = self
        nameField.shouldReturn = {
            (fieldView : FieldView) -> Bool in
            
            if __w_self != nil {
                __w_self!.priceField.textField.becomeFirstResponder()
            }
            return true
        }
        
        priceField.shouldReturn = {
            (fieldView : FieldView) -> Bool in
            
            if __w_self != nil {
                __w_self!.countField.textField.becomeFirstResponder()
            }
            return true
        }
        
        countField.shouldReturn = {
            (fieldView : FieldView) -> Bool in
            
            if __w_self != nil {
                __w_self!.countField.textField.resignFirstResponder()
            }
            return true
        }
        
        priceField.shouldChangeValue = {(fieldView : FieldView, newValue : String) -> Bool in
            let priceString = __w_self?.priceString(newValue)
            if priceString == nil {
                return false
            }
            if priceString?.characters.count == 0 {
                return true
            }
            let price = Double(priceString!)
            return price != nil
        }
        
        countField.shouldChangeValue = {(fieldView : FieldView, newValue : String) -> Bool in
            if newValue.isEmpty {
                return true
            }
            let set = NSCharacterSet.decimalDigitCharacterSet().invertedSet
            return newValue.rangeOfCharacterFromSet(set) == nil
        }
        
        let didChange = {(fieldView : FieldView) -> Void in
            if __w_self != nil {
                __w_self?.updateSaveButton()
            }
        }
        nameField.didChange = didChange
        priceField.didChange = didChange
        countField.didChange = didChange
        
        if item != nil {
            nameField.textField.text = item.name
            priceField.textField.text = item.priceString
            countField.textField.text = "\(item.count ?? 0)"
        } else {
            priceField?.textField?.text = Item.priceString(0)
        }
    }
    
    // MARK: Actions
    
    @IBAction func onCancel(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSave(sender: AnyObject) {
        let identifier = item?.identifier
        let count = Int(countField.textField.text!)!
        
        var parameters : [String : AnyObject]
        parameters = ["name" : nameField.textField.text!, "identifier" : identifier ?? NSUUID().UUIDString, "price" : priceString(priceField.textField.text!) ?? 0, "count" : count]
        
        weak var __w_self = self
        DatabaseManager.sharedManager().performAsyncBlock({ () -> Void in
            var error : NSError?
            DatabaseManager.sharedManager().updateObjects([parameters], entityName: kEntityItem, error: &error, shouldClear: false)
            
            }) { () -> Void in
                if __w_self != nil {
                    __w_self?.navigationController?.popToRootViewControllerAnimated(true)
                }
        }
    }
    
    @IBAction func onTap(sender: AnyObject) {
        view.endEditing(true)
    }
    
    func updateSaveButton() {
        var enabled = true
        
        for textField in [nameField, priceField, countField] {
            if let text = textField.textField.text {
                enabled = !text.isEmpty
            } else {
                enabled = false
            }
            if !enabled {
                break;
            }
        }
        if enabled && item != nil {
            enabled = nameField.textField.text != item.name || priceString(priceField.textField.text!) != priceString(item.priceString) || Int(countField.textField.text!) != item.count;
        }
        navigationItem.rightBarButtonItem?.enabled = enabled
    }
}

class FieldView: UIView, UITextFieldDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var shouldChangeValue:Optional< (fieldView: FieldView, newValue : String) -> Bool> = nil
    var shouldReturn:Optional< (fieldView: FieldView) -> Bool> = nil
    var didChange:Optional< (fieldView: FieldView) -> Void> = nil
   
    @IBAction func onChangeValue(sender: AnyObject) {
        if didChange != nil {
            didChange!(fieldView: self)
        }
    }
    
    // MARK: Text Field
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
       
        if shouldChangeValue != nil {
            let start = textField.text?.startIndex.advancedBy(range.location)
            let end = start?.advancedBy(range.length)
            
            if start != nil && end != nil {
                let swiftRange = Range<String.Index>(start: start!, end: end!)
                
                let newValue = textField.text!.stringByReplacingCharactersInRange(swiftRange, withString: string)
                return shouldChangeValue!(fieldView : self, newValue: newValue)
            }
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if shouldReturn != nil {
            return shouldReturn!(fieldView : self)
        }
        return true
    }
}
