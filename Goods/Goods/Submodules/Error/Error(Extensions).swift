//
//  Error(Extensions).swift
//  Goods
//

import Foundation
import UIKit

extension UIViewController {
    
    public var topPresentedViewController : UIViewController {
        if self.presentedViewController != nil && self.presentedViewController?.isBeingDismissed() != false {
            return self.presentedViewController!.topPresentedViewController
        }
        return self;
    }
}

extension NSError {
    
    public convenience init(description : String) {
        self.init(domain: "com.goods.app", code: 0, userInfo: [NSLocalizedDescriptionKey : description])
    }
    
    public func handle() {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            let alert = UIAlertController(title: self.localizedDescription, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.Cancel, handler: nil))
            
            let controller = UIApplication.sharedApplication().keyWindow?.rootViewController?.topPresentedViewController
            
            controller?.presentViewController(alert, animated: true, completion: nil)
        }
    }
}