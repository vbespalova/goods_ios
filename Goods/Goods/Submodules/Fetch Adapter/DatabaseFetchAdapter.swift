//
//  DatabaseFetchAdapter.swift
//  

import Foundation
import UIKit

class DatabaseFetchAdapter : FetchAdapter, NSFetchedResultsControllerDelegate {
    var _entityName : String
    var _predicate : NSPredicate!
    var _sortDescriptors :[NSSortDescriptor]
    var _moc : NSManagedObjectContext
    
    lazy var _fetchController : NSFetchedResultsController = {
        let request = NSFetchRequest(entityName: self._entityName)
        request.predicate = self._predicate
        request.sortDescriptors = self._sortDescriptors
        
        var controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self._moc, sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = self
        return controller
    }()
    
    init (entityName: String, predicate: NSPredicate!, sortDescriptors : [NSSortDescriptor], moc: NSManagedObjectContext) {
        _entityName = entityName
        _predicate = predicate
        _sortDescriptors = sortDescriptors
        _moc = moc
        super.init()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        _fetchController.delegate = self
        super.viewWillAppear()
    }
    
    override func viewWillDisappear() {
        _fetchController.delegate = self
        super.viewWillDisappear()
    }
    
    override func fetch() {
        do {
           try _fetchController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        super.fetch()
    }
    
    override var sectionsCount : Int {
        return (_fetchController.sections?.count)!
    }
    
    override var fetchedObjects : [AnyObject]{
        return _fetchController.fetchedObjects!
    }
    
    override func rowsCountInSection(section: Int) -> Int {
        return (_fetchController.sections?[section])!.numberOfObjects
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath) -> AnyObject? {
        return _fetchController.objectAtIndexPath(indexPath)
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        if tableView != nil {
            tableView?.beginUpdates()
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        if tableView != nil {
            switch (type) {
            case NSFetchedResultsChangeType.Insert :
                tableView?.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
            case NSFetchedResultsChangeType.Delete :
                tableView?.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
            default:
                break
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        if tableView != nil {
            switch (type) {
            case NSFetchedResultsChangeType.Insert :
                tableView?.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            case NSFetchedResultsChangeType.Delete:
                tableView?.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            case NSFetchedResultsChangeType.Update :
                tableView?.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            case NSFetchedResultsChangeType.Move :
                tableView?.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
                tableView?.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
                
            }
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView?.endUpdates()
        fetched()
    }
}
