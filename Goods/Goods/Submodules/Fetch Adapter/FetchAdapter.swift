//
//  FetchAdapter.swift
//  Goods
//

import Foundation
import UIKit

public class FetchAdapter : NSObject{
    public var tableView: UITableView!
    public var emptyView: UIView!
    
    public var onFetched : Optional <() -> ()> = nil
    
    public func viewDidLoad() {
        fetch()
    }
    
    public func viewWillAppear() {
    }
    
    public func viewWillDisappear() {
    }
    
    public func fetch() {
        if tableView != nil {
            tableView.reloadData()
        }
        fetched()
    }
    
    public func fetched () {
        if emptyView != nil {
            emptyView.hidden = fetchedObjectsCount > 0
        }
        if onFetched != nil {
            onFetched!()
        }
    }

    public var fetchedObjects : [AnyObject]{
        get {
            var value = [AnyObject]()
            for i in 0...sectionsCount {
                for j in 0...rowsCountInSection(i) {
                    let indexPath = NSIndexPath(forItem: j, inSection: i)
                    let object = objectAtIndexPath(indexPath)
                    if object != nil {
                        value.append(object!)
                    }
                    
                }
            }
            return value
        }
    }
    
    public var fetchedObjectsCount : Int {
        get {
            return fetchedObjects.count
        }
    }
    
    public var sectionsCount : Int {
        return 0
    }
    
    public func rowsCountInSection(section: Int) ->Int {
        return 0
    }
    
    public func objectAtIndexPath(indexPath :NSIndexPath) ->AnyObject? {
        return nil;
    }
}