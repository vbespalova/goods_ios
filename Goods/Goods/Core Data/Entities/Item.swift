//
//  Item.swift
//  Goods
//

import Foundation
import CoreData

@objc(Item)
class Item: NSManagedObject, NSManagedObjectParsing {

    func updateWithParameters(parameters: [NSObject : AnyObject]!) -> NSError! {
        
        var mParameters = parameters
        if let count = parameters["count"] as? String {
            mParameters["count"] = Int(count)
        }
        if let price = parameters["price"] as? String {
            mParameters["price"] = Double(price)
        }
        return self._updateWithParameters(mParameters)
    }
    
    var priceString : String {
        return Item.priceString(self.price)
    }
    
    class func priceString (price : NSDecimalNumber?) -> String {
        return "\(price ?? 0) \(self.currency)"
    }
    
    class var currency : String {
        return "руб."
    }
}
