//
//  Item+CoreDataProperties.swift
//  Goods
//

import Foundation
import CoreData

extension Item {

    @NSManaged var identifier: String?
    @NSManaged var name: String?
    @NSManaged var price: NSDecimalNumber?
    @NSManaged var count: NSNumber?

}
