//
//  Goods DatabaseManagerConfigurator.swift
//  Goods

import UIKit

let kEntityItem = "Item"

class GoodsDatabaseManagerConfigurator: DatabaseManagerConfigurator {

    override func databaseName() -> String! {
        return "Goods"
    }
}
